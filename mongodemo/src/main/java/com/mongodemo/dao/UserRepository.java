package com.mongodemo.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.mongodemo.document.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

}
