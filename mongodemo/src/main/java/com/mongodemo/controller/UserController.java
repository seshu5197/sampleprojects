package com.mongodemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodemo.document.ResponseObject;
import com.mongodemo.document.User;
import com.mongodemo.service.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/")
	@CrossOrigin(origins = "*")
	public ResponseEntity<ResponseObject> saveUser(@RequestBody User user)
	{

		return new ResponseEntity<ResponseObject>(userService.add(user),HttpStatus.OK);
	}

	@GetMapping("/")
	@CrossOrigin(origins = "*")
	public ResponseEntity<List<User>> getUsers()
	{
		List<User> users = userService.findAll();
		if(users != null)
		{
			return new ResponseEntity<List<User>>(users,HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<List<User>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/{id}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<ResponseObject> deleteUser(@PathVariable String id)
	{

		return new ResponseEntity<ResponseObject>(userService.remove(id),HttpStatus.OK);

	}

	@GetMapping("/{id}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<ResponseObject> getUser(@PathVariable String id)
	{
		ResponseObject respopnse = userService.findById(id);
		return new ResponseEntity<ResponseObject>(respopnse,HttpStatus.OK);

	}

	@PutMapping("/{id}")
	@CrossOrigin(origins = "*")
	public ResponseEntity<ResponseObject> updateUser(@PathVariable String id , @RequestBody User user)
	{
		ResponseObject result = userService.update(user , id);

		return new ResponseEntity<ResponseObject>(result,HttpStatus.OK);

	}
}
