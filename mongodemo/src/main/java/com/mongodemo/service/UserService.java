package com.mongodemo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodemo.dao.UserRepository;
import com.mongodemo.document.ResponseObject;
import com.mongodemo.document.User;

@Service
public class UserService {


	@Autowired
	private UserRepository userRepository;

	private ResponseObject responseObject = null;

	public ResponseObject add(User user) {
		responseObject = new ResponseObject();
		if(userRepository.save(user)!=null)
		{
			responseObject.setMessage("Added Successfully");
			return responseObject;
		}

		responseObject.setMessage("Addition Failed");
		return responseObject;
	}

	public List<User> findAll() {


		return userRepository.findAll();
	}

	public ResponseObject findById(String id)
	{
		responseObject = new ResponseObject();
		Optional<User> optional =  userRepository.findById(id);
		if(optional.isPresent())
		{
			responseObject.setResponse(optional.get());
			responseObject.setMessage("Fetched user Details suuccessfully");
			return responseObject;
		}
		else
		{
			responseObject.setResponse(null);
			responseObject.setMessage("No user found");
			return responseObject;
		}
	}
	public ResponseObject remove(String id)
	{
		ResponseObject responseObject = findById(id);
		User user = (User) responseObject.getResponse();
		responseObject.setResponse(null);
		if(user == null)
		{
			responseObject.setMessage("No user Found");
			return responseObject;	
		}
		userRepository.delete(user);
		responseObject.setMessage("deleted successfully");
		return responseObject;
	}

	public ResponseObject update(User userUpdate , String id)
	{
		ResponseObject responseObject = findById(id);
		User user = (User) responseObject.getResponse();
		responseObject.setResponse(null);
		if(user == null)
		{
			responseObject.setMessage("No user found with this id");
			return responseObject;
		}

		if(userUpdate.getName() != null)
		{
			if(userUpdate.getName().isEmpty())
			{
				responseObject.setMessage("Name is empty");
				return responseObject;
			}
			user.setName(userUpdate.getName());
		}
		if(userUpdate.getAge() != null)
		{
			if(userUpdate.getAge()== 0)
			{
				responseObject.setMessage("Age cannot be zero");
				return responseObject;
			}
			user.setAge(userUpdate.getAge());
		}
		if(userUpdate.getAddress() != null)
		{
			
			user.setAddress(userUpdate.getAddress());
		}

		if(userRepository.save(user) != null)
		{
			responseObject.setMessage("Updated successfully");
			return responseObject;
		}
		else
		{
			responseObject.setMessage("Updation failed");
			return responseObject;
		}
	}
}
