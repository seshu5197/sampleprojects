package com.abc.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abc.mybatis.User;
import com.abc.mybatis.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(path="/insert",method=RequestMethod.POST,consumes="application/json")
	public ResponseEntity<String> insertUser(@RequestBody User user)
	{
		System.out.println("inside controller");
		if(userService.insertUser(user))
		{
			return new ResponseEntity<String>("Inserted successfully",HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<String>("Insertion failed",HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/get")
	public String getUser()
	{
		System.out.println("inside controller get");
		
			return "hello";
		
	}

}
