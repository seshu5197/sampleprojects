package com.abc.mybatis.service;

import java.util.List;

import com.abc.mybatis.User;

public interface UserMapper {

	public void insertUser(User user);
	 
	 public User getUserById(Integer userId);
	 
	 public List<User> getAllUsers();
	 
	 public void updateUser(User user);
	 
	 public void deleteUser(Integer userId);
	
}
