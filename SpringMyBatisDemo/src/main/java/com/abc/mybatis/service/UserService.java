package com.abc.mybatis.service;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

import com.abc.mybatis.User;
import com.abc.mybatis.configurations.UserConfig;

@Service
public class UserService {
	
	 public boolean insertUser(User user) {
		  SqlSession sqlSession = UserConfig.getSqlSessionFactory().openSession();
		  try{
		  UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
		  userMapper.insertUser(user);
		  sqlSession.commit();
		  return true;
		  } catch(Exception exception) {
			  System.out.println(exception);
			  return false;
		  }
		  finally{
		   sqlSession.close();
		  }
		  
		 }

}
